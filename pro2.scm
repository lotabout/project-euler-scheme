#!/usr/bin/env gosh

(use srfi-1) ; import `filter` which we have implement one in pro1.scm

; generate a list of fibonacci numbers which <= `limit`
(define (genfibo high-bound)
  (let rec ((cur 1) (last 1) (rslt '()))
    (if (> cur high-bound)
      (reverse! rslt)
      (rec (+ cur last) cur (cons cur rslt)))))

; test of gen
; (genfibo 90)

(define (main args)
  (print (apply + (filter even? (genfibo 4000000)))))
