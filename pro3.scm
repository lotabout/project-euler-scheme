#!/usr/bin/env gosh

; Use Sieve of Eratosthenes to generate a list of primes.
(define (sieve upper-bound)
  (let ((numbers (make-vector (floor upper-bound) #t)))
    (define (next-prime cur-prime) ; get the next prime number from array
      (let ((next-index (+ cur-prime 1)))
        (cond
          ((>= next-index upper-bound) #f)
          ((vector-ref numbers next-index) next-index)
          (else (next-prime next-index)))))
    (define (strike! prime) ; strike off all multiples of prime in the array
      (let rec ((prime prime) (next (+ prime prime)))
        (if (< next upper-bound)
          (begin
            (vector-set! numbers next #f)
            (rec prime (+ next prime))))))
    (define (collect) ; collect all prime numbers vec[x]=#t => collect x
      (let rec ((cur-index (- upper-bound 1)) (rslt '()))
        (cond 
          ((< cur-index 2) rslt)
          ((vector-ref numbers cur-index)
           (rec (- cur-index 1) (cons cur-index rslt)))
          (else (rec (- cur-index 1) rslt)))))
    (let iter ((prime 2)) ; do the sieve
      (if prime
        (begin
          (strike! prime)
          (iter (next-prime prime)))))
    (collect))) ; collect the result

; test of sieve
;(sieve 100)

(define (main args)
  (display
   (let* ((num 600851475143)
          (primes (reverse! (sieve (floor->exact (sqrt num))))))
    (let rec ((i (car primes)) (primes (cdr primes)))
      (if (= (modulo num i) 0)
        i
        (rec (car primes) (cdr primes))))))
  (newline))
