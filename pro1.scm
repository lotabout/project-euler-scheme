#!/usr/bin/env gosh

(define (filter test lst)
  (let rec ((lst lst) (rslt '()))
    (cond
      ((null? lst) (reverse! rslt))
      ((test (car lst)) 
       (rec (cdr lst) (cons (car lst) rslt)))
      (else (rec (cdr lst) rslt)))))

; if input is a multiple of 3 or 5 => true
; else => false
(define (multiple3/5? item)
  (or (= (modulo item 5) 0)
          (= (modulo item 3) 0)))

; test of multiple3/5?
; (print (filter multiple3/5? '(1 2 3 4 5 6 7 8 9)))

; generate a list in [a b]
; (genlist 1 n) => (1 2 ... n)
(define (genlist low high)
  (let rec ((cur high) (rslt '()))
    (if (< cur low)
      rslt
      (rec (- cur 1) (cons cur rslt)))))
; test of genlist
; (genlist 1 10)
; (genlist 4 4)
; (genlist 4 3)

(define (main arg)
  (print (apply + (filter multiple3/5? (genlist 1 999)))))
